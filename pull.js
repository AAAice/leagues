(function ($) {
    $('button').on('click', function () {
        $('.content ul').remove();
        $('<i class="fa fa-refresh fa-spin"/>').appendTo('body');
        
        var zip = $('select option:selected').text().substring(1, 6);

        $.getJSON('http://data.colorado.gov/resource/4ykn-tg5h.json?entitystatus=Good%20Standing&principalzipcode=' + zip, function (data) {
              
            var items = [],
                $ul;
            
            $.each(data, function (key, val) {
                items.push('<li id="' + key + '"><span class="name">' + val.entityname + '</span><br><span class="addr">' + val.principaladdress1 + '</span> <span class="city">' + val.principalcity + '</span></li>');
            });
            if (items.length < 1) {
                items.push('<li>No results for this ZIP code, try again!</li>');
            }
            
            $('.fa-spin').remove();
            
            $ul = $('<ul />').appendTo('.content');
            
            $ul.append(items);
        });
    });
}(jQuery));